using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    //public attribute
    public float speed = 5;
    public Text text;

    //To access other components  we need a reference to them
    private Transform t;

    //LYFECICLE!
    //We have no access to main method.
    //We can inject logic to particular game objects on specific moments.

    //Awake runs upon component creation. Runs before start.
    void Awake()
    {
        //print("Awake");
        //get a reference to any component IN THE SAME GAME OBJECT
        t = GetComponent<Transform>();
    }

    //Start is also called once after ALL the awake methods have been called.
    //Start only runs in objects that are enabled.
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    //The game engine (and any graphical app) runs in a loop.
    //Loops does two things: run logic and update graphics (frame).
    //The idea is to run the loop as many times as you can in a second.
    //Frames per second (fps) = framerate.
    //Ideal is +60 fps
    //OK is 30 fps (the least to consider for a real time app).

    //1 rule for update: do only the following:
    //    - check input from user
    //    - move stuff
    void Update()
    {
        //how to capture input
        //poll directly to the devices
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("DOWN");
        }

        if (Input.GetKey(KeyCode.A))
        {
            print("KEY");
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            print("UP");
        }
        //MOUSE
        if (Input.GetMouseButtonDown(0))
        {
            print("MOUSE DOWN");
        }

        if (Input.GetMouseButton(0))
        {
            print("MOUSE");
        }

        if (Input.GetMouseButtonUp(0))
        {
            print("MOUSE UP");
        }

        //using axes
        //retrieve a float value
        //0 is neutral
        //range  = [-1,1]
        float h = Input.GetAxis("Horizontal"); //You can use GetAxisRaw and its not softer
        float v = Input.GetAxis("Vertical");

        //Move the object
        t.Translate(speed*h * Time.deltaTime, speed*v * Time.deltaTime, 0, Space.World);
    }
       
    //Late update also happens in each frame
    //After ALL the updates
    void LateUpdate()
    {
        //Debug.Log("Late update")
    }

    //Will run in a set amount of time, which we configure on Unity under Edit/Time/FixedTimeStep
    void FixedUpdate()
    {
        //Debug.Log("Fixed Update")
    }

    //Collisions between objects using the physics engine
    //Requirements:
    //- all the objects involved MUST hvae a collider
    //at least one of them has a rigid body component (suscribes object to physics engine)
    //the rigidbidy enabled object must be moving (otherwise is considered asleeep and not taken into account)

    //three moments in the life of a collision
    
    //invoked in the frame in which 2 objects start touching
    void OnCollisionEnter(Collision c)
    {
        //Collision is an object that contains info of teh collision!
        //such as:
        //- a reference to the other object involved
        //- pints of collision
        //etc

        print("COLLISION IS WORKING" + c.transform.name);
        print("COLLISION IS WORKING" + c.transform.gameObject.tag);
        print("COLLISION IS WORKING" + c.transform.gameObject.layer);
        text.text = "A trigger happened";
    }

    //Invoked after the frame that they started touching and while they still touch
    void OnCollisionStay(Collision c)
    {
        print("COLLISION STAY");
    }

    //invoked the first frame after the objects stopped touching
    void OnCollisionExit(Collision c)
    {
        print("COLLISION EXIT");
    }

    //TRIGGER
    //You can use triggers if you don't want any physical reaction but still want to be able to interact with an object

    void OnTriggerEnter(Collider c) 
    {
        print("TRIGGER ENTER");
    }

    void OnTriggerStay(Collider c) {
        print("TRIGGER STAY");
    }

    void OnTriggerExit(Collider c) 
    {
        Debug.Log("TRIGGER EXIT");
    }

}
    

